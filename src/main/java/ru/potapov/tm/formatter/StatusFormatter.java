package ru.potapov.tm.formatter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.Formatter;
import ru.potapov.tm.enumeration.Status;

import java.text.ParseException;
import java.util.Locale;

public class StatusFormatter implements Formatter<Status> {
    @Override
    public Status parse(@Nullable final String s, @Nullable Locale locale) throws ParseException {
        return Status.valueOf(s);
    }

    @Override
    public String print(@NotNull final Status status, @Nullable Locale locale) {
        return status.toString();
    }
}
