package ru.potapov.tm.formatter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.Formatter;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.ParseException;
import java.util.Locale;

public class RoleFormatter implements Formatter<RoleType> {
    @Override
    public RoleType parse(@Nullable final String s, @Nullable Locale locale) throws ParseException {
        return RoleType.valueOf(s);
    }

    @Override
    public String print(@NotNull final RoleType roleType, @Nullable Locale locale) {
        return roleType.toString();
    }
}
