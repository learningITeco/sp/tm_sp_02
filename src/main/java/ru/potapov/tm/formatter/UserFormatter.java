package ru.potapov.tm.formatter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.User;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class UserFormatter implements Formatter<User> {

    @NotNull ServiceLocator serviceLocator;

    public UserFormatter(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public User parse(@Nullable final String s, @Nullable Locale locale) throws ParseException {
        Collection<User> list = serviceLocator.getUserService().findAll();
        for (User user : list) {
            if (user.getId().equals(s))
                return user;
        }

        return new User();
    }

    @Override
    public String print(@NotNull final User user, @Nullable Locale locale) {
        return user.toString();
    }
}
