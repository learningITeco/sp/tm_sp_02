package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;

public interface IUserService extends IService<User> {
    @NotNull
    User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role);
}
