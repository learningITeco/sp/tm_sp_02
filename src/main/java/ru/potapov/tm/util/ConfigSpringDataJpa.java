package ru.potapov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class ConfigSpringDataJpa {

    @Bean
    public DataSource dataSource(
            @Value("${spring.datasource.driver-class-name}") final String dataSourceDriver,
            @Value("${spring.datasource.url}") final String dataSourceUrl,
            @Value("${spring.datasource.user}") final String dataSourceUser,
            @Value("${spring.datasource.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${spring.jpa.show-sql}") final boolean showSql,
            @Value("${spring.jpa.hibernate.ddl-auto}") final String tableStrategy,
            @Value("${spring.jpa.properties.hibernate.dialect}") final String dialect
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.potapov.tm");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Autowired @NotNull LocalContainerEntityManagerFactoryBean emf ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }

//    @Bean
//    public EntityManagerFactory createEntityManagerFactory(@Autowired LocalContainerEntityManagerFactoryBean containerEntityManagerFactoryBean){
//        return containerEntityManagerFactoryBean.getObject();
//    }

    @Bean
    public EntityManager createEntityManager(@Autowired LocalContainerEntityManagerFactoryBean emf){
        return emf.getObject().createEntityManager();
    }
}
