package ru.potapov.tm.util;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.formatter.ProjectFormatter;
import ru.potapov.tm.formatter.RoleFormatter;
import ru.potapov.tm.formatter.StatusFormatter;
import ru.potapov.tm.formatter.UserFormatter;

@Setter
@Getter
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "ru.potapov.tm")
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    @NotNull private ServiceLocator serviceLocator;

    @Bean
    public InternalResourceViewResolver resolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new UserFormatter(serviceLocator));
        registry.addFormatter(new ProjectFormatter(serviceLocator));
        registry.addFormatter(new StatusFormatter());
        registry.addFormatter(new RoleFormatter());
    }
}
