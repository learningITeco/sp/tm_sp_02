package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.util.SignatureUtil;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Override
    public @Nullable Session generateSession(@NotNull Session sessionUser, @NotNull String userId) {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = (Session)sessionUser.clone(); }catch (Exception e){return null;}
        sessionUserClone.setSignature("");
        sessionUser.setSignature(SignatureUtil.sign(sessionUserClone,"", 1));
        sessionUser.setUser(getServiceLocator().getUserService().findOne(userId));
        //addSession(sessionUser, null);
        return sessionUser;
    }
}
