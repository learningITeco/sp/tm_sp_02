package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.IService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.AbstractEntity;

import java.util.Collection;

@Service
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Autowired
    @Nullable IRepository<T> repository;

    @Autowired
    @Nullable ServiceLocator serviceLocator;

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        if (repository == null)
            return null;

        return repository.findOne(id);
    }

    @Override
    public @NotNull Collection<T> findAll() {
        if (repository == null)
            return null;

        return repository.findAll();
    }

    @Override
    public void merge(@NotNull final T t) {
        if (repository == null)
            return;

        repository.merge(t);
    }

    @Override
    @Transactional
    public void persist(@NotNull final T t) {
        if (repository == null)
            return;

            repository.persist(t);
    }

    @Override
    public void remove(@NotNull final T t) {
        if (repository == null)
            return;

        repository.remove(t);
    }

    @Override
    public void removeAll() {
        if (repository == null)
            return;

        repository.removeAll();
    }
}
