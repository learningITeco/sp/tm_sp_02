package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.entity.Task;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService<Task> implements ITaskService {
}
