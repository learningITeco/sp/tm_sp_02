package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.entity.Project;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService<Project> implements IProjectService {
}
