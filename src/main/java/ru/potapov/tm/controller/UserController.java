package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
//@RequestMapping(value = "/user")
@ComponentScan(basePackages = "ru.potapov.tm")
public class UserController {

    @NotNull private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public UserController() {
        System.out.println(serviceLocator);
    }

    // list page
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String showAllUsers(Model model) {
        logger.debug("showAllUsers()");
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        return "users/list";
    }

    //users
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated @NotNull User user,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {
        logger.debug("saveOrUpdateUser() : {}", user);

        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "users/userform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(user.isNew()){
                redirectAttributes.addFlashAttribute("msg", "User added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
            }
            serviceLocator.getUserService().merge(user);

            // POST/FORWARD/GET
            populateDefaultModel(model);
            return "users/list";
        }
    }

    // show add user form
    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String showAddUserForm(Model model) {
        logger.debug("showAddUserForm()");

        @NotNull  User user = new User();
        // set default value
        user.setLogin("");
        user.setRoleType(RoleType.User);
        user.setHashPass("fdfdffdfdfdfdfdfdfs");
        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";
    }

    // show update form
    @RequestMapping(value = "/users/{id}/update", method = RequestMethod.GET)
    public String showUpdateUserForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateUserForm() : {}", id);

        @NotNull final User user = serviceLocator.getUserService().findOne(id);
        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";
    }

    // delete user
    @RequestMapping(value = "/users/{id}/delete", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteUser() : {}", id);

        @NotNull final User user = serviceLocator.getUserService().findOne(id);
        if (user != null)
            serviceLocator.getUserService().remove(user);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "User is deleted!");

        populateDefaultModel(model);

        return "users/list";
    }

     private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
