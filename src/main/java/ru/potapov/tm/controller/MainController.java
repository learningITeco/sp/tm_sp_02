package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
//@RequestMapping(value = "/")
@ComponentScan(basePackages = "ru.potapov.tm")
public class MainController {

    @NotNull private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public MainController() {
        System.out.println(serviceLocator);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(){
        if (serviceLocator == null)
            return null;

        if (!serviceLocator.isInit())
            serviceLocator.init();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("start");

        return modelAndView;
    }

    private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
