package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.User;

import javax.persistence.TypedQuery;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class UserRepository extends AbstractRepository<User> {
    @Nullable
    @Override
    public User findOne(@NotNull String id) {
        @Nullable User user = null;
        @Nullable final String query = "from User where id =:id";
        @NotNull TypedQuery<User> typedQuery = entityManager.createQuery(query, User.class).setParameter("id", id);
        user = typedQuery.getSingleResult();

        return user;
    }

    @Override
    public @NotNull Collection<User> findAll() {
        @NotNull  TypedQuery<User> typedQuery = entityManager.createQuery("From User", User.class);
        @NotNull final Collection<User> collection = typedQuery.getResultList();
        return collection;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM User ");
    }
}
