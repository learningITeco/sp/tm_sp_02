package ru.potapov.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Session;

@Repository
public interface ISessionRepository extends CrudRepository<Session, String> {

}
