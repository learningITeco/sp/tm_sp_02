package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Task;

import javax.persistence.TypedQuery;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class TaskRepository extends AbstractRepository<Task> {
    @Nullable
    @Override
    public Task findOne(@NotNull String id) {
        @Nullable Task task = null;
        @Nullable final String query = "from Task where id =:id";
        @NotNull TypedQuery<Task> typedQuery = entityManager.createQuery(query, Task.class).setParameter("id", id);
        task = typedQuery.getSingleResult();

        return task;
    }

    @Override
    public @NotNull Collection<Task> findAll() {
        @NotNull  TypedQuery<Task> typedQuery = entityManager.createQuery("From Task", Task.class);
        @NotNull final Collection<Task> collection = typedQuery.getResultList();
        return collection;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task ");
    }
}
