package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.User;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
    @Nullable User findOptionalByLogin(@NotNull final String login);
}
