package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Session;

import javax.persistence.TypedQuery;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class SessionRepository extends AbstractRepository<Session> {
    @Nullable
    @Override
    public Session findOne(@NotNull String id) {
        @Nullable Session session = null;
        @Nullable final String query = "from Session where session_id =:id";
        @NotNull TypedQuery<Session> typedQuery = entityManager.createQuery(query, Session.class).setParameter("id", id);
        session = typedQuery.getSingleResult();

        return session;
    }

    @Override
    public @NotNull Collection<Session> findAll() {
        @NotNull  TypedQuery<Session> typedQuery = entityManager.createQuery("From Session", Session.class);
        @NotNull final Collection<Session> collection = typedQuery.getResultList();
        return collection;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Session ");
    }
}
